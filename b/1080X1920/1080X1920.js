(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [
		{name:"1080X1920_atlas_", frames: [[0,0,3713,2113],[3715,0,160,158]]}
];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.formular1 = function() {
	this.spriteSheet = ss["1080X1920_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.logo = function() {
	this.spriteSheet = ss["1080X1920_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



// stage content:
(lib._1080X1920 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EhUXiV/MCovAAAMAAAEr/MiovAAAg");
	this.shape.setTransform(540,960);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(5,1,1).p("A/kpfMA/JAAAIAAS/Mg/JAAAg");
	this.shape_1.setTransform(288.1,1436.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhcDLQghgTgSglQgTglAAgxQAAgyATgjQASgmAhgTQAggUApAAQArAAAeAWQAfAVAMAjIAAhKIBCAAIAAE8IhCAAIAAhKQgMAjgfAWQgeAVgrAAQgpAAgggUgAhEgQQgaAbAAAyQAAAwAaAcQAaAcArAAQAbAAAWgNQAWgNANgXQAMgYAAgfQAAgggMgYQgNgWgWgOQgWgNgbABQgrAAgaAbgAg/iZQgXgYAAgnIAAgGIAiAAQAAAtA0gBQAaAAAOgLQANgMAAgVIAiAAIAAAGQAAAngXAYQgYAXgoAAQgnAAgYgXg");
	this.shape_2.setTransform(431.8,1432.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AA2DGQgzAAgZgZQgagYAAg5IAAiaIgtAAIAAg4IAtAAIAAhPIBBAAIAABPIBNAAIAAA4IhNAAIAACbQAAAbAKAKQAKALAaAAIAfAAIAAA5g");
	this.shape_3.setTransform(401.6,1434.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AhZChIAAk8IBCAAIAAA/QAPgfAegTQAdgSAnAAIAABHIgTAAQgrAAgaAWQgZAVAAAuIAAChg");
	this.shape_4.setTransform(381.2,1438);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AhPCPQgjgUgUgkQgUglAAgyQAAgwAUglQAUglAjgUQAkgTAtgBQAuAAAjAUQAjATASAiQATAhAAAqQAAAPgCAMIjzAAQACAxAZAYQAaAXAlAAQAgAAAWgRQAXgQAFgcIBGAAQgGAhgUAbQgUAZggAPQghAOgnAAQgtABgkgUgAg+hVQgZAXgDAuICzAAQABgfgMgVQgLgVgVgKQgUgKgYAAQgmAAgaAYg");
	this.shape_5.setTransform(350.3,1438.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgtDcIAAkDIgsAAIAAg4IAsAAIAAgLQgBg6AjgdQAggdBFADIAAA6QgngCgPAOQgQAOAAAiIAAAGIBGAAIAAA4IhGAAIAAEDg");
	this.shape_6.setTransform(322.2,1432.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhRCPQglgUgVgkQgVgmAAgxQAAgwAVglQAVglAlgUQAkgTAtgBQAuABAkATQAmAUAUAlQAVAlAAAwQAAAygVAlQgUAkgmAUQgkAUgugBQgtABgkgUgAgthdQgWAMgNAYQgMAXAAAiQAAAjAMAXQANAYAWALQAUAMAZAAQAZAAAVgMQAWgLAMgYQANgXAAgjQAAgigNgXQgMgYgWgMQgVgMgZAAQgZAAgUAMg");
	this.shape_7.setTransform(293.9,1438.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhPCPQgjgUgUgkQgUglAAgyQAAgwAUglQAUglAjgUQAkgTAtgBQAuAAAjAUQAjATASAiQATAhAAAqQAAAPgCAMIjzAAQACAxAZAYQAaAXAlAAQAgAAAWgRQAXgQAFgcIBGAAQgGAhgUAbQgUAZggAPQghAOgnAAQgtABgkgUgAg+hVQgZAXgDAuICzAAQABgfgMgVQgLgVgVgKQgUgKgYAAQgmAAgaAYg");
	this.shape_8.setTransform(244.1,1438.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AhZChIAAk8IBCAAIAAA/QAPgfAegTQAdgSAnAAIAABHIgTAAQgrAAgaAWQgZAVAAAuIAAChg");
	this.shape_9.setTransform(216.3,1438);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AhPCPQgjgUgUgkQgUglAAgyQAAgwAUglQAUglAjgUQAkgTAtgBQAuAAAjAUQAjATASAiQATAhAAAqQAAAPgCAMIjzAAQACAxAZAYQAaAXAlAAQAgAAAWgRQAXgQAFgcIBGAAQgGAhgUAbQgUAZggAPQghAOgnAAQgtABgkgUgAg+hVQgZAXgDAuICzAAQABgfgMgVQgLgVgVgKQgUgKgYAAQgmAAgaAYg");
	this.shape_10.setTransform(185.4,1438.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AhjC1QgtgbgXguQgZgvAAg9QAAg7AZgvQAXgvAtgaQAsgbA5AAQBKAAAyAmQAyAmARBBIhGAAQgPglgfgVQgfgWguAAQgiAAgdASQgcASgRAhQgQAhgBArQABAsAQAhQARAhAcASQAdASAiAAQAuAAAfgWQAfgVAPgkIBGAAQgRBAgyAmQgyAmhKAAQg5AAgsgag");
	this.shape_11.setTransform(145.7,1433.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Ah2DuQgmgYgXguQgVgsgBg+QABg8AVgsQAXguAmgYQAogXAwAAQAxAAAjAXQAiAXAPAnIAAjTIBiAAIAAIFIhiAAIAAhRQgPAogiAXQgjAXgxAAQgwAAgogXgAhJgTQgcAdAAA0QAAA1AcAeQAcAeAuAAQAtgBAdgeQAcgeAAg0QAAgzgcgeQgdgegtAAQguAAgcAeg");
	this.shape_12.setTransform(924.9,1248.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgxEUIAAmCIBiAAIAAGCgAgsi3QgQgQAAgWQAAgZAQgPQASgOAaAAQAbAAASAOQAQAPAAAZQAAAWgQAQQgSAOgbAAQgaAAgSgOg");
	this.shape_13.setTransform(892.8,1246.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AjIEhIAAo8IBiAAIAABQQAPgnAigXQAjgXAwAAQAxAAAnAYQAoAYAVAtQAXAtAAA9QAAA9gXAsQgVAtgoAYQgnAYgxAAQgwAAgjgYQgigXgPgoIAAELgAhJirQgdAeAAA0QAAAzAdAeQAdAeAsAAQAuAAAbgdQAcgeABg0QgBg1gcgeQgbgeguAAQgsAAgdAfg");
	this.shape_14.setTransform(860.7,1263.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("Ah1CvQgogYgWgtQgVgtAAg9QAAg8AVgtQAWgtAogYQAngYAxAAQAwAAAjAXQAjAXAPAnIAAhQIBhAAIAAGDIhhAAIAAhSQgPAogjAXQgjAYgwAAQgxAAgngYgAhIhSQgcAeAAA0QAAA0AcAeQAbAeAtAAQAtAAAdgfQAdgeABgzQgBgzgdgeQgdgfgtAAQgtAAgbAeg");
	this.shape_15.setTransform(811.4,1254.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("Ah1DFIAAmDIBjAAIAABKQAUglAkgVQAkgWAsAAIAABqIgdAAQhrAAAABkIAAC7g");
	this.shape_16.setTransform(775.9,1254.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("Ah2CvQgngYgWgtQgWgtABg9QgBg8AWgtQAWgtAngYQAngYAyAAQAwAAAiAXQAkAXAPAnIAAhQIBiAAIAAGDIhiAAIAAhSQgPAogkAXQgiAYgwAAQgyAAgngYgAhJhSQgcAeAAA0QAAA0AcAeQAcAeAtAAQAtAAAegfQAdgeAAgzQAAgzgdgeQgegfgtAAQgtAAgcAeg");
	this.shape_17.setTransform(719.8,1254.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("ABEDyQhBAAgiggQgigfAAhKIAAinIg2AAIAAhTIA2AAIAAhgIBhAAIAABgIBXAAIAABTIhXAAIAACpQAAAcAMALQALALAbAAIAmAAIAABVg");
	this.shape_18.setTransform(682.5,1249.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AhhCvQgsgZgYgsQgZgtAAg9QAAg8AZgtQAYgtAsgYQAtgYA3AAQBLAAAyApQAyAoAMBGIhnAAQgHgfgWgRQgXgSgiAAQgmAAgbAdQgaAdAAA3QAAA4AaAdQAbAdAmAAQAiAAAXgSQAWgSAHgfIBnAAQgMBHgyAoQgyAphLAAQg3AAgtgYg");
	this.shape_19.setTransform(647.7,1254.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("Ah1CvQgngYgXgtQgVgtgBg9QABg8AVgtQAXgtAngYQAngYAwAAQAxAAAjAXQAjAXAOAnIAAhQIBiAAIAAGDIhiAAIAAhSQgOAogjAXQgjAYgxAAQgwAAgngYgAhIhSQgcAeAAA0QAAA0AcAeQAbAeAuAAQAsAAAdgfQAdgeAAgzQAAgzgdgeQgdgfgsAAQguAAgbAeg");
	this.shape_20.setTransform(601.5,1254.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("ABEDyQhBAAghggQgjgfAAhKIAAinIg2AAIAAhTIA2AAIAAhgIBiAAIAABgIBWAAIAABTIhWAAIAACpQgBAcALALQAMALAaAAIAnAAIAABVg");
	this.shape_21.setTransform(564.2,1249.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("ABaDFIAAjWQABgugYgZQgYgagpAAQgpAAgZAcQgaAbAAAzIAADNIhiAAIAAmDIBiAAIAABOQAQgoAjgVQAkgXAtAAQBDAAAoAsQAoAtAABQIAADgg");
	this.shape_22.setTransform(527.9,1254.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AhlCvQgtgZgagsQgagtAAg9QAAg8AagtQAagtAtgYQAtgYA4AAQA4AAAtAYQAuAYAaAtQAaAtAAA8QAAA9gaAtQgaAsguAZQgtAYg4AAQg4AAgtgYgAhFhTQgdAdAAA2QAAA3AdAdQAdAdAoAAQApAAAdgdQAcgdAAg3QAAg2gcgdQgdgegpAAQgoAAgdAeg");
	this.shape_23.setTransform(482.1,1254.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AhhCvQgsgZgYgsQgZgtAAg9QAAg8AZgtQAYgtAsgYQAtgYA3AAQBLAAAyApQAyAoAMBGIhnAAQgHgfgWgRQgXgSgiAAQgnAAgaAdQgaAdAAA3QAAA4AaAdQAaAdAnAAQAiAAAXgSQAWgSAHgfIBnAAQgMBHgyAoQgyAphLAAQg3AAgtgYg");
	this.shape_24.setTransform(438.1,1254.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("ADhDFIAAjWQAAgsgXgYQgWgZgoAAQgqAAgZAaQgXAaAAAyIAADNIhiAAIAAjWQAAgsgXgYQgWgZgoAAQgqAAgYAaQgZAaAAAyIAADNIhiAAIAAmDIBiAAIAABJQAQglAhgVQAhgVAsAAQAvAAAjAXQAhAWASAqQATgoAjgXQAlgYAsAAQBHAAApAsQApAsAABRIAADgg");
	this.shape_25.setTransform(364.8,1254.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AhlCvQgtgZgagsQgagtAAg9QAAg8AagtQAagtAtgYQAtgYA4AAQA4AAAtAYQAuAYAaAtQAaAtAAA8QAAA9gaAtQgaAsguAZQgtAYg4AAQg4AAgtgYgAhFhTQgdAdAAA2QAAA3AdAdQAdAdAoAAQApAAAdgdQAcgdAAg3QAAg2gcgdQgdgegpAAQgoAAgdAeg");
	this.shape_26.setTransform(305.6,1254.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("Ag7DCIiQmDIBqAAIBiEgIBikgIBpAAIiQGDg");
	this.shape_27.setTransform(261.8,1254.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AhiCvQgtgZgYgsQgYgtAAg9QAAg8AYgtQAYgtAtgYQAsgYA3AAQA6AAArAYQArAYAYApQAXAqAAA1QAAAOgEATIkbAAQACA0AaAYQAaAZAnAAQAhAAAXgRQAWgRAHgdIBoAAQgHApgaAgQgZAggoASQgnATgxAAQg3AAgsgYgAhAhgQgcAZgDAvIC7AAQADgvgbgZQgbgYgoAAQgnAAgaAYg");
	this.shape_28.setTransform(203.6,1254.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("ABFDyQhCAAghggQgjgfAAhKIAAinIg2AAIAAhTIA2AAIAAhgIBiAAIAABgIBWAAIAABTIhWAAIAACpQgBAcALALQAMALAaAAIAnAAIAABVg");
	this.shape_29.setTransform(167.2,1249.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgxEUIAAmCIBiAAIAAGCgAgri3QgRgQAAgWQAAgZARgPQARgOAaAAQAbAAASAOQAQAPAAAZQAAAWgQAQQgSAOgbAAQgaAAgRgOg");
	this.shape_30.setTransform(129.4,1246.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgfEAQAhAAABgvIAAgHIgmAAIAAhIIBPAAQAKAeAAAjQAAAvgWAaQgWAagpgBgAhnBGQgugigKg+IBfAAQADAcAWAQQAWAQAhAAQAaAAANgMQAOgMAAgTQAAgRgMgKQgMgKgRgGQgRgGgegHQgsgKgbgKQgbgKgTgXQgUgWABgnQAAgzAmggQAngeBEAAQBFAAArAjQAqAjALA8IhdAAQgFgcgUgRQgTgRgeAAQgaAAgOANQgOALAAAVQAAAQALAJQAMAKAQAGQARAFAfAJQArAJAdALQAbALAUAXQAUAYAAAoQAAAwgnAfQgnAfhCAAQhKAAgtgig");
	this.shape_31.setTransform(101.1,1264.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgwEDIAAoFIBhAAIAAIFg");
	this.shape_32.setTransform(946,1161.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AiUCYQgogsAAhPIAAjhIBhAAIAADWQAAAuAZAZQAYAaAoAAQAqABAZgcQAZgcAAgzIAAjNIBjAAIAAGDIhjAAIAAhOQgPAngjAXQgkAVgtAAQhDAAgogsg");
	this.shape_33.setTransform(913.3,1168.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("Ah1DFIAAmDIBjAAIAABKQAUglAkgVQAkgWAsAAIAABqIgdAAQhrAAAABkIAAC7g");
	this.shape_34.setTransform(877.8,1167.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("Ah1CvQgngYgXgtQgVgtAAg9QAAg8AVgtQAXgtAngYQAngYAxAAQAwAAAjAXQAjAXAOAnIAAhQIBiAAIAAGDIhiAAIAAhSQgOAogjAXQgjAYgwAAQgxAAgngYgAhIhSQgcAeAAA0QAAA0AcAeQAbAeAtAAQAtAAAdgfQAdgeAAgzQAAgzgdgeQgdgfgtAAQgtAAgbAeg");
	this.shape_35.setTransform(837,1168.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgwEDIAAoFIBhAAIAAIFg");
	this.shape_36.setTransform(804.8,1161.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AiUCYQgogsAAhPIAAjhIBhAAIAADWQAAAuAZAZQAYAaAoAAQApABAagcQAZgcAAgzIAAjNIBjAAIAAGDIhjAAIAAhOQgPAngjAXQgkAVgtAAQhDAAgogsg");
	this.shape_37.setTransform(772.1,1168.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("ADhDFIAAjWQAAgsgXgYQgXgZgoAAQgpAAgYAaQgZAaAAAyIAADNIhhAAIAAjWQAAgsgWgYQgXgZgoAAQgpAAgZAaQgYAaAAAyIAADNIhjAAIAAmDIBjAAIAABJQAPglAhgVQAhgVAtAAQAvAAAiAXQAhAWATAqQARgoAlgXQAjgYAuAAQBGAAApAsQApAsAABRIAADgg");
	this.shape_38.setTransform(712.9,1167.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("Ah1DFIAAmDIBjAAIAABKQAUglAkgVQAkgWAsAAIAABqIgdAAQhrAAAABkIAAC7g");
	this.shape_39.setTransform(663.3,1167.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AhlCvQgtgZgagsQgagtAAg9QAAg8AagtQAagtAtgYQAtgYA4AAQA4AAAtAYQAuAYAaAtQAaAtAAA8QAAA9gaAtQgaAsguAZQgtAYg4AAQg4AAgtgYgAhFhTQgdAdAAA2QAAA3AdAdQAdAdAoAAQApAAAdgdQAcgdAAg3QAAg2gcgdQgdgegpAAQgoAAgdAeg");
	this.shape_40.setTransform(624.7,1168.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("Ag/EOIAAkvIgxAAIAAhUIAxAAIAAgJQAAhFAogmQAngkBNgBIAUABIAABVQgrgCgSAPQgRAPAAAnIAAAAIBOAAIAABUIhOAAIAAEvg");
	this.shape_41.setTransform(589.5,1160.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("Ah1D8QgogXgWguQgVgtAAg9QAAg8AVgtQAWgtAogYQAmgXAyAAQAwAAAjAXQAjAXAPAnIAAhRIBhAAIAAGDIhhAAIAAhRQgPAngjAYQgjAXgwAAQgyAAgmgYgAhIgEQgcAdAAA0QAAA1AcAdQAbAeAtAAQAtAAAdgeQAdgfABgzQgBg0gdgdQgdgegtAAQgtAAgbAegAhQi3QgfgeAAg2IAAgIIAwAAQAAA0A/AAQA/AAAAg0IAwAAIAAAIQAAA2geAeQgeAfgzAAQgzAAgdgfg");
	this.shape_42.setTransform(537.4,1160.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AiUDCIAAhPICxjiIitAAIAAhSIEeAAIAABOIi2DjIC9AAIAABSg");
	this.shape_43.setTransform(498,1168.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("Ah2CvQgmgYgXgtQgWgtAAg9QAAg8AWgtQAXgtAmgYQAogYAwAAQAxAAAjAXQAjAXAOAnIAAhQIBiAAIAAGDIhiAAIAAhSQgOAogjAXQgjAYgxAAQgwAAgogYgAhJhSQgcAeAAA0QAAA0AcAeQAcAeAuAAQAtAAAdgfQAcgeAAgzQAAgzgcgeQgdgfgtAAQguAAgcAeg");
	this.shape_44.setTransform(456.9,1168.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AhiCvQgtgZgYgsQgYgtAAg9QAAg8AYgtQAYgtAtgYQAsgYA3AAQA6AAArAYQArAYAXApQAYAqAAA1QAAAOgEATIkbAAQACA0AaAYQAaAZAnAAQAhAAAXgRQAWgRAHgdIBoAAQgHApgaAgQgZAggoASQgnATgxAAQg3AAgsgYgAhBhgQgbAZgDAvIC8AAQABgvgagZQgbgYgoAAQgnAAgbAYg");
	this.shape_45.setTransform(412.5,1168.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("ABEDyQhBAAghggQgjgfAAhKIAAinIg2AAIAAhTIA2AAIAAhgIBiAAIAABgIBWAAIAABTIhWAAIAACpQgBAcALALQAMALAaAAIAnAAIAABVg");
	this.shape_46.setTransform(376.1,1163.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AhiCvQgsgZgZgsQgYgtAAg9QAAg8AYgtQAZgtAsgYQAsgYA4AAQA5AAArAYQArAYAXApQAYAqAAA1QAAAOgDATIkcAAQADA0AZAYQAaAZAnAAQAhAAAXgRQAXgRAGgdIBoAAQgIApgZAgQgZAggnASQgoATgwAAQg4AAgsgYgAhBhgQgaAZgEAvIC8AAQACgvgbgZQgbgYgnAAQgoAAgbAYg");
	this.shape_47.setTransform(341.2,1168.2);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgwEDIAAoFIBhAAIAAIFg");
	this.shape_48.setTransform(310,1161.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AjJEhIAAo8IBjAAIAABQQAPgnAjgXQAigXAwAAQAxAAAnAYQAnAYAXAtQAVAtAAA9QAAA9gVAsQgXAtgnAYQgnAYgxAAQgwAAgigYQgjgXgPgoIAAELgAhJirQgeAeAAA0QAAAzAeAeQAcAeAtAAQAtAAAcgdQAcgeAAg0QAAg1gcgeQgcgegtAAQgtAAgcAfg");
	this.shape_49.setTransform(278,1177.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("ADhDFIAAjWQAAgsgXgYQgWgZgoAAQgqAAgYAaQgYAaAAAyIAADNIhiAAIAAjWQAAgsgXgYQgWgZgoAAQgqAAgYAaQgZAaAAAyIAADNIhiAAIAAmDIBiAAIAABJQAQglAhgVQAhgVAsAAQAvAAAjAXQAiAWARAqQATgoAjgXQAlgYAsAAQBHAAApAsQApAsAABRIAADgg");
	this.shape_50.setTransform(216.8,1167.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AhlCvQgtgZgagsQgagtAAg9QAAg8AagtQAagtAtgYQAtgYA4AAQA4AAAtAYQAuAYAaAtQAaAtAAA8QAAA9gaAtQgaAsguAZQgtAYg4AAQg4AAgtgYgAhFhTQgdAdAAA2QAAA3AdAdQAdAdAoAAQApAAAdgdQAcgdAAg3QAAg2gcgdQgdgegpAAQgoAAgdAeg");
	this.shape_51.setTransform(157.6,1168.2);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("Ah6DcQg2gggeg5Qgdg6AAhJQAAhIAdg5QAeg6A2gfQA2ghBGAAQBdAAA9AwQA9AvATBTIhoAAQgQgpghgWQgigYgwAAQgmAAggAUQgfATgSAlQgRAkAAAwQAAAwARAlQASAlAfATQAgAUAmAAQAwAAAigYQAhgWAQgpIBoAAQgTBTg9AvQg9AwhdAAQhFgBg3gfg");
	this.shape_52.setTransform(108.6,1162.9);

	this.instance = new lib.logo();
	this.instance.parent = this;
	this.instance.setTransform(86,1689);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#EF7D00").s().p("AiqHBIAAixIC1AAIAACxgAigCqIAAjmIBEAAQBgAAA2geQA2gdAAhPQAAg1gegdQgegegzAAQg2AAggAgQggAgAAAyIigAAQgChIAhg5QAig5BBghQBCghBZAAQB6AABKBBQBLBBAAB3QAAB4hNA/QhMA/h/ACIAAB5g");
	this.shape_53.setTransform(957.9,1005.2);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#EF7D00").s().p("AjKGxQhDgpgmhNQglhOAAhoQAAhoAlhNQAmhOBDgoQBDgpBUAAQBUAAA7AoQA9AnAZBDIAAiKICnAAIAAKYIinAAIAAiLQgZBDg9ApQg7AnhUAAQhUAAhDgogAh9gIQgwAzAABaQAABaAwAzQAwAzBOAAQBNAAAyg1QAyg0AAhXQAAhZgygzQgyg0hNAAQhOAAgwAzgAiLk6Qg0g0AAhcIAAgOIBUAAQAABZBrAAQBsAAAAhZIBUAAIAAAOQAABcg0A0Qg0A1hYAAQhXAAg0g1g");
	this.shape_54.setTransform(885,1003.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#EF7D00").s().p("AB1GfQhwAAg6g2Qg7g3AAh9IAAkfIhdAAIAAiPIBdAAIAAikICoAAIAACkICUAAIAACPIiUAAIAAEhQAAAwATATQATATAuAAIBCAAIAACSg");
	this.shape_55.setTransform(821.1,1008.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#EF7D00").s().p("AjKEsQhDgpgmhNQglhNAAhoQAAhoAlhOQAmhOBDgnQBDgpBUgBQBUAAA7AoQA9AnAZBDIAAiJICnAAIAAKYIinAAIAAiMQgZBEg9AoQg7AohUAAQhUgBhDgogAh9iNQgwA0AABaQAABZAwAzQAwAzBOAAQBNAAAyg1QAyg0AAhWQAAhZgyg0Qgyg0hNAAQhOAAgwAzg");
	this.shape_56.setTransform(756.3,1016.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#EF7D00").s().p("Aj/FNIAAiGIExmGIkqAAIAAiMIHsAAIAACFIk5GFIFFAAIAACOg");
	this.shape_57.setTransform(688.9,1016.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#EF7D00").s().p("AhUHaIAAqYICoAAIAAKYgAhLk8QgdgZABgoQgBgoAdgaQAegaAtAAQAvAAAdAaQAdAagBAoQABAogdAZQgdAagvAAQgtAAgegag");
	this.shape_58.setTransform(644.4,1002.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#EF7D00").s().p("AhTG8IAAt3ICnAAIAAN3g");
	this.shape_59.setTransform(612.1,1005.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#EF7D00").s().p("AjKEsQhDgpgmhNQglhNAAhoQAAhoAlhOQAmhOBDgnQBDgpBUgBQBUAAA7AoQA9AnAZBDIAAiJICnAAIAAKYIinAAIAAiMQgZBEg9AoQg7AohUAAQhUgBhDgogAh9iNQgwA0AABaQAABZAwAzQAwAzBOAAQBNAAAyg1QAyg0AAhWQAAhZgyg0Qgyg0hNAAQhOAAgwAzg");
	this.shape_60.setTransform(553.7,1016.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#EF7D00").s().p("ACcFRIAAluQAAhPgpgsQgpgshGAAQhIAAgrAvQgrAvAABYIAAFfIipAAIAAqYICpAAIAACGQAbhEA8glQA9gmBOAAQB0AABEBLQBEBMAACJIAAGBg");
	this.shape_61.setTransform(475.2,1016.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#EF7D00").s().p("AiuErQhNgpgshNQgthNAAhnQAAhoAthOQAshOBNgnQBNgpBhgBQBhABBNApQBNAnAtBOQAtBOAABoQAABngtBNQgtBNhNApQhNAqhhAAQhhAAhNgqgAh3iPQgxAxAABfQAABeAxAyQAxAxBGAAQBGAAAygxQAxgyAAheQAAhfgxgxQgygzhGAAQhGAAgxAzg");
	this.shape_62.setTransform(396.7,1016.9);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#EF7D00").s().p("AiyEaQhOg5gRhsICiAAQAGAwAlAcQAlAdA7ABQAsAAAWgWQAXgVAAghQAAgegUgRQgUgTgdgIQgdgLg1gNQhLgRgugRQgugQghgnQghgmAAhDQAAhXBCg2QBCg2B0AAQB4ABBKA9QBJA8ASBnIifAAQgJgxgigcQghgdg1AAQgsAAgYAUQgYAUAAAkQAAAbAUASQATAQAdAJQAcAKA1ANQBMARAvATQAwARAiAoQAiAoAABGQAABUhDA1QhCA1hxAAQiAgBhOg6g");
	this.shape_63.setTransform(326,1016.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#EF7D00").s().p("AjJFRIAAqYICpAAIAACAQAkhAA9glQA9gkBMAAIAAC1IgxAAQi5AAAACsIAAFAg");
	this.shape_64.setTransform(271.5,1016.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#EF7D00").s().p("AipErQhMgpgqhNQgphNAAhnQAAhoAphOQAqhOBMgnQBMgpBggBQBiABBKApQBKAnAoBIQAoBIAABbQAAAYgGAgInmAAQAEBZAsAqQAsArBDAAQA5AAAngeQAngcALgxICzAAQgNBFgsA4QgrA3hEAeQhEAghSAAQhgAAhMgqgAhwilQguArgFBRIFCAAQADhRgtgrQgvgqhDABQhEgBgvAqg");
	this.shape_65.setTransform(206.4,1016.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#EF7D00").s().p("AlZHvIAAvVICpAAIAACKQAahDA7gnQA7goBTAAQBVAABCApQBEAoAmBNQAmBOgBBpQABBogmBNQgmBNhEApQhCAohVAAQhTAAg7gnQg7gpgahDIAAHIgAh/kmQgyA0AABZQAABXAyA0QAyA0BNAAQBOAAAwgzQAwgzAAhZQAAhbgwgzQgwg0hOABQhNAAgyA0g");
	this.shape_66.setTransform(130.3,1032.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#EF7D00").s().p("AjKGxQhDgpgmhNQglhOAAhoQAAhoAlhNQAmhOBDgoQBDgpBUAAQBUAAA7AoQA9AnAZBDIAAiKICnAAIAAKYIinAAIAAiLQgZBDg9ApQg7AnhUAAQhUAAhDgogAh9gIQgwAzAABaQAABaAwAzQAwAzBOAAQBNAAAyg1QAyg0AAhXQAAhZgygzQgyg0hNAAQhOAAgwAzgAiLk6Qg0g0AAhcIAAgOIBUAAQAABZBrAAQBsAAAAhZIBUAAIAAAOQAABcg0A0Qg0A1hYAAQhXAAg0g1g");
	this.shape_67.setTransform(788.8,829.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#EF7D00").s().p("AB1GeQhwAAg6g2Qg7g1AAh/IAAkfIhdAAIAAiOIBdAAIAAilICoAAIAAClICUAAIAACOIiUAAIAAEiQAAAwATATQATATAuAAIBCAAIAACRg");
	this.shape_68.setTransform(724.9,834.2);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#EF7D00").s().p("AjJFRIAAqYICpAAIAACAQAkhAA+glQA8gkBMAAIAAC1IgxAAQi5AAAACsIAAFAg");
	this.shape_69.setTransform(680.4,841.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#EF7D00").s().p("AipEsQhMgqgqhMQgphOAAhnQAAhoAphOQAqhOBMgoQBMgoBgAAQBiAABKAoQBKApAoBHQAoBHAABcQAAAZgGAfInmAAQAEBaAsApQAsAqBDAAQA5AAAngdQAngcALgyICzAAQgNBGgsA3QgrA4hEAfQhEAehSAAQhgAAhMgogAhwilQguAqgFBSIFCAAQADhSgtgqQgvgqhDAAQhEAAgvAqg");
	this.shape_70.setTransform(615.3,842.4);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#EF7D00").s().p("AhsHQIAAoJIhVAAIAAiPIBVAAIAAgQQAAh2BEhBQBDhACFAAIAiABIAACTQhKgFgeAbQgeAaAABCIAAABICGAAIAACPIiGAAIAAIJg");
	this.shape_71.setTransform(556.2,829.3);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#EF7D00").s().p("AiuEsQhNgqgshMQgthOAAhnQAAhoAthOQAshOBNgoQBNgoBhAAQBhAABNAoQBNAoAtBOQAtBOAABoQAABngtBOQgtBMhNAqQhNAohhAAQhhAAhNgogAh3iQQgxAyAABfQAABeAxAyQAxAxBGAAQBGAAAygxQAxgyAAheQAAhegxgzQgygxhGAAQhGAAgxAxg");
	this.shape_72.setTransform(496.7,842.4);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#EF7D00").s().p("AiuEsQhNgqgshMQgthOAAhnQAAhoAthOQAshOBNgoQBNgoBhAAQBhAABNAoQBNAoAtBOQAtBOAABoQAABngtBOQgtBMhNAqQhNAohhAAQhhAAhNgogAh3iQQgxAyAABfQAABeAxAyQAxAxBGAAQBGAAAygxQAxgyAAheQAAhegxgzQgygxhGAAQhGAAgxAxg");
	this.shape_73.setTransform(393.8,842.4);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#EF7D00").s().p("AhUHaIAAqYICoAAIAAKYgAhLk8QgdgZABgnQgBgqAdgaQAegZAtAAQAvAAAdAZQAcAaAAAqQAAAngcAZQgdAbgvAAQgtAAgegbg");
	this.shape_74.setTransform(313.1,828.3);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#EF7D00").s().p("AipEsQhMgqgqhMQgphOAAhnQAAhoAphOQAqhOBMgoQBMgoBgAAQBiAABKAoQBKApAoBHQAoBHAABcQAAAZgGAfInmAAQAEBaAsApQAsAqBDAAQA5AAAngdQAngcALgyICzAAQgNBGgsA3QgrA4hEAfQhEAehSAAQhgAAhMgogAhwilQguAqgFBSIFCAAQADhSgtgqQgvgqhDAAQhEAAgvAqg");
	this.shape_75.setTransform(259.7,842.4);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#EF7D00").s().p("AjJFRIAAqYICpAAIAACAQAkhAA+glQA9gkBLAAIAAC1IgxAAQi5AAAACsIAAFAg");
	this.shape_76.setTransform(200.5,841.9);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#EF7D00").s().p("AhsGoIkrtPIC0AAIDjKeIDkqeIC1AAIktNPg");
	this.shape_77.setTransform(130.3,833.3);

	this.instance_1 = new lib.formular1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-67,0,0.309,0.309);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.instance},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(473,959,1148,1922);
// library properties:
lib.properties = {
	width: 1080,
	height: 1920,
	fps: 24,
	color: "#EEEEEE",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/1080X1920_atlas_.png", id:"1080X1920_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;