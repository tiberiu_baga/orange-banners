(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [
		{name:"970X250_atlas_", frames: [[0,0,3713,2113],[3715,0,160,158]]}
];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.formular1 = function() {
	this.spriteSheet = ss["970X250_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.logo = function() {
	this.spriteSheet = ss["970X250_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



// stage content:
(lib._970X250 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EhLxgThMCXjAAAMAAAAnDMiXjAAAg");
	this.shape.setTransform(485,125);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(2,1,1).p("AouinIRdAAIAAFPIxdAAg");
	this.shape_1.setTransform(181.6,208.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgZA4QgJgFgFgLQgFgJAAgOQAAgOAFgJQAFgKAJgGQAJgFALAAQALAAAJAGQAJAFADAKIAAgUIASAAIAABXIgSAAIAAgVQgDAKgJAGQgJAFgLABQgLgBgJgFgAgSgEQgHAHAAAOQAAANAHAIQAHAHALAAQAIAAAGgDQAGgEADgGQAEgHAAgIQAAgJgEgHQgDgFgGgDQgGgEgIAAQgLAAgHAHgAgQgqQgHgGAAgLIAAgCIAJAAQAAAMAOAAQAHAAAEgDQADgDAAgGIAKAAIAAACQAAALgGAGQgHAHgLAAQgKAAgGgHg");
	this.shape_2.setTransform(221.3,207.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAPA3QgOAAgHgHQgGgHAAgPIAAgqIgNAAIAAgQIANAAIAAgWIARAAIAAAWIAVAAIAAAQIgVAAIAAAqQAAAHADADQACADAHAAIAJAAIAAAQg");
	this.shape_3.setTransform(212.9,208);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgYAtIAAhXIASAAIAAARQAEgIAIgGQAIgEALAAIAAATIgGAAQgLAAgIAGQgGAGAAAMIAAAtg");
	this.shape_4.setTransform(207.3,209);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgVAnQgKgFgFgKQgGgKAAgOQAAgNAGgKQAFgKAKgFQAKgGAMAAQAMAAAKAFQAKAGAFAJQAFAJAAAMIgBAHIhCAAQAAANAIAHQAGAGAKAAQAIAAAHgFQAGgEABgIIAUAAQgCAKgFAHQgGAHgJAEQgIAEgLAAQgMAAgKgGgAgQgXQgIAHAAAMIAxAAQAAgIgEgGQgCgGgHgDQgFgCgGAAQgLAAgGAGg");
	this.shape_5.setTransform(198.8,209.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgMA9IAAhHIgMAAIAAgQIAMAAIAAgDQAAgQAJgIQAJgIATABIAAAQQgLAAgEAEQgFADAAAKIAAABIAUAAIAAAQIgUAAIAABHg");
	this.shape_6.setTransform(191,207.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgWAnQgKgFgGgKQgFgKAAgOQAAgNAFgKQAGgKAKgFQAKgGAMAAQANAAAKAGQAKAFAGAKQAFAKAAANQAAAOgFAKQgGAKgKAFQgKAGgNAAQgMAAgKgGgAgMgZQgGADgEAHQgDAGAAAJQAAAKADAGQAEAHAGADQAGADAGAAQAHAAAGgDQAGgDADgHQAEgGAAgKQAAgJgEgGQgDgHgGgDQgGgDgHAAQgGAAgGADg");
	this.shape_7.setTransform(183.2,209.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgVAnQgKgFgFgKQgGgKAAgOQAAgNAGgKQAFgKAKgFQAJgGANAAQAMAAAKAFQAJAGAGAJQAFAJAAAMIgBAHIhCAAQAAANAIAHQAGAGAKAAQAIAAAHgFQAGgEABgIIAUAAQgCAKgFAHQgGAHgJAEQgIAEgLAAQgNAAgJgGgAgQgXQgIAHAAAMIAwAAQABgIgEgGQgCgGgHgDQgFgCgGAAQgLAAgGAGg");
	this.shape_8.setTransform(169.4,209.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgYAtIAAhXIASAAIAAARQAFgIAHgGQAIgEALAAIAAATIgGAAQgMAAgHAGQgGAGAAAMIAAAtg");
	this.shape_9.setTransform(161.7,209);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgVAnQgKgFgFgKQgGgKAAgOQAAgNAGgKQAFgKAKgFQAJgGANAAQAMAAAKAFQAJAGAGAJQAFAJAAAMIgBAHIhCAAQAAANAIAHQAGAGAKAAQAIAAAHgFQAGgEABgIIAUAAQgCAKgFAHQgGAHgJAEQgIAEgLAAQgNAAgJgGgAgQgXQgIAHAAAMIAwAAQABgIgEgGQgCgGgHgDQgFgCgGAAQgLAAgGAGg");
	this.shape_10.setTransform(153.2,209.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgbAxQgMgGgHgOQgGgMAAgRQAAgPAGgNQAHgOAMgHQAMgHAQAAQAUAAAOAKQAOALAEASIgTAAQgEgKgIgHQgKgFgMgBQgJABgHAFQgJAEgEAKQgFAJABALQgBAMAFAJQAEAJAJAFQAHAFAJAAQAMAAAKgGQAIgGAEgKIATAAQgEASgOAKQgOALgUAAQgQAAgMgIg");
	this.shape_11.setTransform(142.2,207.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgkBLQgMgIgHgOQgHgOAAgTQAAgTAHgOQAHgOAMgHQAMgIAQAAQAOAAALAIQALAHAFAMIAAhCIAeAAIAACiIgeAAIAAgaQgFANgLAHQgLAHgOAAQgQAAgMgHgAgWgFQgJAIAAARQAAAQAJAKQAJAJANAAQAOAAAJgKQAKgJAAgQQAAgQgKgJQgJgKgOAAQgNAAgJAKg");
	this.shape_12.setTransform(565.1,87.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgPBXIAAh5IAeAAIAAB5gAgNg5QgFgEAAgIQAAgHAFgFQAFgFAIAAQAIAAAGAFQAFAFAAAHQAAAIgFAEQgGAFgIAAQgIAAgFgFg");
	this.shape_13.setTransform(555,87.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("Ag/BbIAAizIAgAAIAAAZQAEgMALgHQALgIAPAAQAPAAAMAIQAMAHAIAOQAGAPABATQgBATgGANQgIAOgMAIQgMAHgPAAQgPAAgLgHQgLgHgEgNIAABUgAgWg1QgKAKABAQQgBAQAKAJQAJAJANAAQAOAAAJgIQAJgKAAgQQAAgRgJgJQgJgKgOAAQgNAAgJAKg");
	this.shape_14.setTransform(545,92.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgkA3QgMgIgHgOQgHgOAAgTQAAgSAHgOQAHgOAMgIQAMgHAQAAQAOAAALAHQALAHAFANIAAgaIAeAAIAAB5IgeAAIAAgZQgFAMgLAHQgLAIgOAAQgQAAgMgIgAgWgZQgJAKAAAPQAAAQAJAKQAJAJANAAQAOAAAJgJQAKgKAAgQQAAgPgKgKQgJgJgOAAQgNAAgJAJg");
	this.shape_15.setTransform(529.5,89.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgkA+IAAh5IAfAAIAAAXQAGgLALgHQALgHAOAAIAAAhIgJAAQghAAAAAfIAAA7g");
	this.shape_16.setTransform(518.3,89.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgkA3QgMgIgHgOQgHgOAAgTQAAgSAHgOQAHgOAMgIQAMgHAQAAQAOAAALAHQALAHAFANIAAgaIAeAAIAAB5IgeAAIAAgZQgFAMgLAHQgLAIgOAAQgQAAgMgIgAgWgZQgJAKAAAPQAAAQAJAKQAJAJANAAQAOAAAJgJQAKgKAAgQQAAgPgKgKQgJgJgOAAQgNAAgJAJg");
	this.shape_17.setTransform(500.7,89.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAWBMQgVAAgKgKQgLgKAAgXIAAgzIgRAAIAAgbIARAAIAAgeIAeAAIAAAeIAbAAIAAAbIgbAAIAAA0QAAAIAEAEQAEAEAHAAIAMAAIAAAag");
	this.shape_18.setTransform(489,88.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgeA3QgNgIgJgOQgHgOAAgTQAAgSAHgOQAJgOANgIQAOgHARAAQAYgBAPANQAQANAEAWIghAAQgCgKgHgFQgHgGgLAAQgLAAgIAKQgJAIAAARQAAASAJAJQAIAJALAAQALAAAHgGQAHgFACgKIAhAAQgEAWgQANQgPANgYAAQgRAAgOgIg");
	this.shape_19.setTransform(478.1,89.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgkA3QgMgIgHgOQgHgOAAgTQAAgSAHgOQAHgOAMgIQAMgHAQAAQAOAAALAHQALAHAFANIAAgaIAeAAIAAB5IgeAAIAAgZQgFAMgLAHQgLAIgOAAQgQAAgMgIgAgWgZQgJAKAAAPQAAAQAJAKQAJAJANAAQAOAAAJgJQAKgKAAgQQAAgPgKgKQgJgJgOAAQgNAAgJAJg");
	this.shape_20.setTransform(463.5,89.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAWBMQgVAAgKgKQgLgKAAgXIAAgzIgRAAIAAgbIARAAIAAgeIAeAAIAAAeIAcAAIAAAbIgcAAIAAA0QAAAIADAEQAFAEAHAAIANAAIAAAag");
	this.shape_21.setTransform(451.8,88.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AAdA+IAAhDQgBgOgHgIQgIgIgMAAQgNAAgHAIQgIAJAAAQIAABAIggAAIAAh5IAgAAIAAAYQAFgMAKgHQAMgHANAAQAVAAANAOQAMAOAAAZIAABGg");
	this.shape_22.setTransform(440.4,89.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgfA3QgPgIgHgOQgJgOABgTQgBgSAJgOQAHgOAPgIQAOgHARAAQASAAAOAHQAOAIAIAOQAJAOAAASQAAATgJAOQgIAOgOAIQgOAIgSAAQgRAAgOgIgAgVgZQgJAIAAARQAAASAJAJQAJAIAMABQANgBAJgIQAJgJAAgSQAAgRgJgIQgJgKgNABQgMgBgJAKg");
	this.shape_23.setTransform(426,89.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgeA3QgOgIgHgOQgIgOAAgTQAAgSAIgOQAHgOAOgIQAOgHARAAQAYgBAPANQAQANAEAWIghAAQgCgKgHgFQgHgGgLAAQgLAAgJAKQgHAIgBARQABASAHAJQAJAJALAAQALAAAHgGQAHgFACgKIAhAAQgEAWgQANQgPANgYAAQgRAAgOgIg");
	this.shape_24.setTransform(412.2,89.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("ABHA+IAAhDQAAgOgIgHQgGgIgNAAQgNAAgHAIQgJAIAAAQIAABAIgdAAIAAhDQAAgOgHgHQgIgIgMAAQgNAAgIAIQgHAIAAAQIAABAIggAAIAAh5IAgAAIAAAXQAEgMALgGQALgHANAAQAPAAALAHQAJAHAHANQAFgMALgHQALgIAOAAQAXAAANAOQANAOAAAZIAABGg");
	this.shape_25.setTransform(389.2,89.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgfA3QgPgIgHgOQgJgOABgTQgBgSAJgOQAHgOAPgIQAOgHARAAQASAAAOAHQAOAIAIAOQAJAOAAASQAAATgJAOQgIAOgOAIQgOAIgSAAQgRAAgOgIgAgVgZQgJAIAAARQAAASAJAJQAJAIAMABQANgBAJgIQAJgJAAgSQAAgRgJgIQgJgKgNABQgMgBgJAKg");
	this.shape_26.setTransform(370.6,89.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgSA9Igth5IAhAAIAeBaIAfhaIAhAAIgtB5g");
	this.shape_27.setTransform(356.8,89.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgeA3QgOgIgIgOQgHgOAAgTQAAgSAHgOQAIgOAOgIQAOgHARAAQASAAANAHQAOAIAHANQAHANAAAQIgBAKIhYAAQABAQAIAIQAIAHAMABQAKAAAHgGQAHgFACgJIAhAAQgDANgHAKQgIAKgNAGQgMAGgPAAQgRAAgOgIgAgUgeQgIAJgBAPIA6AAQABgPgJgJQgIgHgMAAQgMAAgJAHg");
	this.shape_28.setTransform(338.5,89.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AAWBMQgVAAgKgKQgLgKAAgXIAAgzIgRAAIAAgbIARAAIAAgeIAeAAIAAAeIAcAAIAAAbIgcAAIAAA0QAAAIAEAEQAEAEAHAAIANAAIAAAag");
	this.shape_29.setTransform(327.1,88.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgPBXIAAh5IAeAAIAAB5gAgNg5QgFgEAAgIQAAgHAFgFQAFgFAIAAQAIAAAGAFQAFAFAAAHQAAAIgFAEQgGAFgIAAQgIAAgFgFg");
	this.shape_30.setTransform(315.2,87.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgJBRQAKAAAAgPIAAgCIgLAAIAAgXIAYAAQADAJAAAMQAAAOgHAIQgHAIgMAAgAggAWQgOgLgDgSIAdAAQACAIAGAFQAHAFAKAAQAIAAAEgEQAEgEAAgFQAAgFgDgDQgEgEgFgBIgOgFIgXgGQgIgDgGgHQgGgHAAgNQAAgPAMgKQAMgKAVAAQAWAAANALQANALAEATIgdAAQgCgJgGgFQgGgFgJAAQgIAAgFADQgEAEAAAGQAAAGADADQAEADAFABIAOAFQAOADAJADQAJAEAGAHQAGAHAAANQAAAOgMAKQgMAKgVAAQgXAAgOgLg");
	this.shape_31.setTransform(306.3,92.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgPBRIAAihIAfAAIAAChg");
	this.shape_32.setTransform(292.6,87.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AguAwQgMgOAAgZIAAhGIAeAAIAABDQAAAOAIAIQAHAIANAAQANAAAHgIQAIgJAAgQIAAhAIAfAAIAAB5IgfAAIAAgYQgEAMgMAHQgLAHgNAAQgVAAgNgOg");
	this.shape_33.setTransform(282.2,89.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgkA+IAAh5IAfAAIAAAXQAGgLALgHQAMgHANAAIAAAhIgJAAQghAAAAAfIAAA7g");
	this.shape_34.setTransform(271.1,89.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgkA3QgMgIgHgOQgHgOAAgTQAAgSAHgOQAHgOAMgIQAMgHAQAAQAOAAALAHQALAHAFANIAAgaIAeAAIAAB5IgeAAIAAgZQgFAMgLAHQgLAIgOAAQgQAAgMgIgAgWgZQgJAKAAAPQAAAQAJAKQAJAJANAAQAOAAAJgJQAKgKAAgQQAAgPgKgKQgJgJgOAAQgNAAgJAJg");
	this.shape_35.setTransform(258.2,89.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgPBRIAAihIAeAAIAAChg");
	this.shape_36.setTransform(248.2,87.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AguAwQgMgOAAgZIAAhGIAeAAIAABDQAAAOAIAIQAHAIANAAQANAAAHgIQAIgJAAgQIAAhAIAfAAIAAB5IgfAAIAAgYQgEAMgMAHQgLAHgNAAQgVAAgNgOg");
	this.shape_37.setTransform(237.8,89.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("ABHA+IAAhDQAAgOgHgHQgIgIgMAAQgNAAgIAIQgHAIgBAQIAABAIgdAAIAAhDQAAgOgIgHQgGgIgNAAQgNAAgHAIQgJAIABAQIAABAIggAAIAAh5IAgAAIAAAXQAEgMALgGQALgHANAAQAPAAALAHQAJAHAHANQAFgMALgHQAMgIAOAAQAWAAANAOQANAOAAAZIAABGg");
	this.shape_38.setTransform(219.3,89.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgkA+IAAh5IAfAAIAAAXQAGgLALgHQAMgHANAAIAAAhIgJAAQghAAAAAfIAAA7g");
	this.shape_39.setTransform(203.7,89.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgfA3QgOgIgJgOQgHgOgBgTQABgSAHgOQAJgOAOgIQAOgHARAAQARAAAPAHQAOAIAIAOQAIAOAAASQAAATgIAOQgIAOgOAIQgPAIgRAAQgRAAgOgIgAgVgZQgJAIAAARQAAASAJAJQAJAIAMABQANgBAJgIQAJgJAAgSQAAgRgJgIQgJgKgNABQgMgBgJAKg");
	this.shape_40.setTransform(191.5,89.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AgTBVIAAheIgQAAIAAgbIAQAAIAAgCQAAgWAMgMQAMgMAYAAIAHAAIAAAbQgOgBgGAFQgFAFAAAMIAZAAIAAAbIgZAAIAABeg");
	this.shape_41.setTransform(180.4,87.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AgkBPQgMgHgHgOQgHgPAAgTQAAgTAHgNQAHgOAMgIQAMgHAQAAQAOAAALAHQALAHAFANIAAgaIAeAAIAAB5IgeAAIAAgZQgFAMgLAHQgLAIgOAAQgQAAgMgIgAgWgBQgJAJAAAQQAAARAJAJQAJAJANAAQAOAAAJgJQAKgKAAgQQAAgQgKgJQgJgJgOAAQgNAAgJAJgAgZg5QgJgJAAgRIAAgDIAPAAQAAAQATAAQAUAAAAgQIAPAAIAAADQAAARgJAJQgKAKgQAAQgPAAgKgKg");
	this.shape_42.setTransform(164,87.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AguA9IAAgYIA3hHIg2AAIAAgaIBaAAIAAAZIg5BGIA7AAIAAAag");
	this.shape_43.setTransform(151.7,89.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgkA3QgMgIgHgOQgHgOAAgTQAAgSAHgOQAHgOAMgIQAMgHAQAAQAOAAALAHQALAHAFANIAAgaIAeAAIAAB5IgeAAIAAgZQgFAMgLAHQgLAIgOAAQgQAAgMgIgAgWgZQgJAKAAAPQAAAQAJAKQAJAJANAAQAOAAAJgJQAKgKAAgQQAAgPgKgKQgJgJgOAAQgNAAgJAJg");
	this.shape_44.setTransform(138.7,89.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AgeA3QgOgIgIgOQgHgOAAgTQAAgSAHgOQAIgOAOgIQAOgHARAAQASAAANAHQAOAIAHANQAHANAAAQIgBAKIhYAAQABAQAIAIQAIAHAMABQAKAAAHgGQAHgFACgJIAhAAQgDANgHAKQgIAKgNAGQgMAGgPAAQgRAAgOgIgAgUgeQgIAJgBAPIA6AAQABgPgJgJQgIgHgMAAQgMAAgJAHg");
	this.shape_45.setTransform(124.8,89.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AAVBMQgUAAgKgKQgLgKAAgXIAAgzIgQAAIAAgbIAQAAIAAgeIAeAAIAAAeIAbAAIAAAbIgbAAIAAA0QAAAIADAEQAFAEAIAAIALAAIAAAag");
	this.shape_46.setTransform(113.4,88.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AgeA3QgOgIgIgOQgHgOAAgTQAAgSAHgOQAIgOAOgIQAOgHARAAQASAAANAHQAOAIAHANQAHANAAAQIgBAKIhYAAQABAQAIAIQAIAHAMABQAKAAAHgGQAHgFACgJIAhAAQgDANgHAKQgIAKgNAGQgMAGgPAAQgRAAgOgIgAgUgeQgIAJgBAPIA6AAQABgPgJgJQgIgHgMAAQgMAAgJAHg");
	this.shape_47.setTransform(102.4,89.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgOBRIAAihIAdAAIAAChg");
	this.shape_48.setTransform(92.6,87.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("Ag+BbIAAizIAeAAIAAAZQAFgMALgHQALgIAPAAQAPAAAMAIQAMAHAHAOQAIAPAAATQAAATgIANQgHAOgMAIQgMAHgPAAQgPAAgLgHQgLgHgFgNIAABUgAgXg1QgJAKAAAQQAAAQAJAJQAKAJANAAQAOAAAJgIQAJgKAAgQQAAgRgJgJQgJgKgOAAQgNAAgKAKg");
	this.shape_49.setTransform(82.6,92.6);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("ABHA+IAAhDQAAgOgIgHQgGgIgNAAQgNAAgIAIQgIAIAAAQIAABAIgdAAIAAhDQAAgOgHgHQgIgIgMAAQgNAAgIAIQgHAIAAAQIAABAIggAAIAAh5IAgAAIAAAXQAEgMALgGQALgHANAAQAPAAALAHQAJAHAHANQAFgMALgHQALgIAOAAQAXAAANAOQANAOAAAZIAABGg");
	this.shape_50.setTransform(63.4,89.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AgfA3QgPgIgHgOQgJgOABgTQgBgSAJgOQAHgOAPgIQAOgHARAAQASAAAOAHQAOAIAIAOQAJAOAAASQAAATgJAOQgIAOgOAIQgOAIgSAAQgRAAgOgIgAgVgZQgJAIAAARQAAASAJAJQAJAIAMABQANgBAJgIQAJgJAAgSQAAgRgJgIQgJgKgNABQgMgBgJAKg");
	this.shape_51.setTransform(44.8,89.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AglBFQgRgKgKgSQgJgSAAgXQAAgWAJgSQAKgSARgKQARgKAVAAQAdAAATAPQATAPAGAaIggAAQgFgNgLgHQgKgIgPAAQgMAAgKAHQgKAGgFALQgGAMAAAOQAAAPAGAMQAFALAKAGQAKAGAMAAQAPAAAKgHQALgHAFgNIAgAAQgGAagTAPQgTAPgdAAQgVAAgRgKg");
	this.shape_52.setTransform(29.3,88.1);

	this.instance = new lib.logo();
	this.instance.parent = this;
	this.instance.setTransform(24,174,0.325,0.325);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#EF7D00").s().p("Ag3CSIAAg5IA7AAIAAA5gAg0A3IAAhKIAXAAQAeAAASgJQARgKAAgaQAAgRgJgJQgKgKgRAAQgRAAgKAKQgLAKAAARIg0AAQgBgXALgTQALgTAWgKQAVgLAcAAQAoAAAYAVQAZAVAAAnQAAAngZAUQgZAVgpAAIAAAng");
	this.shape_53.setTransform(554.7,40);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#EF7D00").s().p("AhBCNQgWgNgMgZQgNgaAAgiQAAgiANgYQAMgZAWgNQAWgOAbAAQAbAAATANQAUANAIAWIAAgtIA3AAIAADYIg3AAIAAguQgIAWgUANQgTANgbAAQgbAAgWgNgAgogCQgQAQAAAdQAAAeAQAQQAQARAZAAQAZAAAQgRQAQgRAAgdQAAgdgQgQQgQgRgZAAQgZAAgQARgAgthmQgRgRAAgeIAAgEIAcAAQAAAdAiAAQAjAAAAgdIAcAAIAAAEQAAAegRARQgRASgdAAQgcAAgRgSg");
	this.shape_54.setTransform(530.9,39.5);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#EF7D00").s().p("AAmCHQgkAAgTgRQgTgSAAgpIAAhdIgeAAIAAgvIAeAAIAAg1IA2AAIAAA1IAxAAIAAAvIgxAAIAABeQAAAPAGAHQAHAGAPAAIAVAAIAAAvg");
	this.shape_55.setTransform(510.1,41.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#EF7D00").s().p("AhBBiQgWgNgMgZQgNgaAAgiQAAghANgZQAMgaAWgNQAWgNAbAAQAbAAATANQAUAMAIAWIAAgtIA3AAIAADYIg3AAIAAgtQgIAWgUANQgTANgbAAQgbAAgWgNgAgogtQgQAQAAAdQAAAeAQAQQAQAQAZAAQAZAAAQgRQAQgQAAgdQAAgcgQgRQgQgRgZAAQgZAAgQARg");
	this.shape_56.setTransform(488.9,43.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#EF7D00").s().p("AhSBsIAAgsIBih+IhgAAIAAguICfAAIAAAsIhlB+IBpAAIAAAug");
	this.shape_57.setTransform(467,43.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#EF7D00").s().p("AgbCbIAAjYIA3AAIAADYgAgYhmQgJgJAAgMQAAgOAJgIQAKgJAOAAQAPAAAKAJQAJAIAAAOQAAAMgJAJQgKAIgPAAQgOAAgKgIg");
	this.shape_58.setTransform(452.5,39.2);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#EF7D00").s().p("AgbCRIAAkhIA3AAIAAEhg");
	this.shape_59.setTransform(441.9,40.2);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#EF7D00").s().p("AhBBiQgWgNgMgZQgNgaAAgiQAAghANgZQAMgaAWgNQAWgNAbAAQAbAAATANQAUAMAIAWIAAgtIA3AAIAADYIg3AAIAAgtQgIAWgUANQgTANgbAAQgbAAgWgNgAgogtQgQAQAAAdQAAAeAQAQQAQAQAZAAQAZAAAQgRQAQgQAAgdQAAgcgQgRQgQgRgZAAQgZAAgQARg");
	this.shape_60.setTransform(422.9,43.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#EF7D00").s().p("AAzBuIAAh3QAAgZgNgPQgOgOgXAAQgWAAgOAPQgOAPAAAdIAAByIg4AAIAAjYIA4AAIAAArQAIgVAUgMQAUgNAYAAQAmAAAWAYQAXAaAAAsIAAB9g");
	this.shape_61.setTransform(397.3,43.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#EF7D00").s().p("Ag4BiQgZgOgPgZQgOgZAAgiQAAghAOgZQAPgaAZgNQAZgNAfAAQAgAAAZANQAZANAPAaQAOAZAAAhQAAAigOAZQgPAZgZAOQgZANggAAQgfAAgZgNgAgmguQgQAQAAAeQAAAfAQAQQAQAQAWAAQAXAAAQgQQAQgQAAgfQAAgegQgQQgQgRgXAAQgWAAgQARg");
	this.shape_62.setTransform(371.7,43.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#EF7D00").s().p("Ag5BcQgagSgFgkIA1AAQABAQAMAJQANAJASAAQAOABAIgIQAHgGAAgLQAAgKgGgFQgHgHgJgCIgagIQgZgFgPgGQgPgFgLgMQgLgMAAgWQAAgdAWgRQAWgSAmAAQAmAAAYAUQAYAUAGAhIg0AAQgDgQgLgJQgLgKgRAAQgOAAgIAIQgHAGAAAMQAAAJAGAFQAGAFAKADIAZAIQAZAFAQAHQAPAFALANQALAMAAAXQAAAcgVARQgWARglAAQgpAAgZgTg");
	this.shape_63.setTransform(348.6,43.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#EF7D00").s().p("AhBBuIAAjYIA4AAIAAApQALgUATgMQAVgMAYAAIAAA7IgQAAQg7AAAAA3IAABpg");
	this.shape_64.setTransform(330.8,43.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#EF7D00").s().p("Ag2BiQgZgOgOgZQgOgZAAgiQAAghAOgZQAOgaAZgNQAYgNAfAAQAgAAAYANQAZANAMAYQANAXABAeQAAAHgCAKIifAAQACAdAOAOQAOAOAXAAQARAAANgJQANgKADgQIA7AAQgEAXgOASQgOASgXAKQgWAKgbAAQgfAAgYgNgAgjg1QgPAOgDAbIBpAAQABgbgPgOQgPgNgWAAQgWAAgOANg");
	this.shape_65.setTransform(309.6,43.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#EF7D00").s().p("AhwChIAAk/IA4AAIAAAtQAIgWATgMQAUgOAaAAQAbAAAWAOQAWANAMAZQANAZAAAjQAAAhgNAZQgMAZgWAOQgWANgbgBQgaABgUgNQgTgOgIgVIAACUgAgphfQgQARAAAdQAAAcAQARQARAQAYAAQAZAAAQgPQAQgRAAgdQAAgegQgQQgQgSgZABQgYgBgRASg");
	this.shape_66.setTransform(284.7,48.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#EF7D00").s().p("AhBCNQgWgNgMgZQgNgaAAgiQAAgiANgYQAMgZAWgNQAWgOAbAAQAbAAATANQAUANAIAWIAAgtIA3AAIAADYIg3AAIAAguQgIAWgUANQgTANgbAAQgbAAgWgNgAgogCQgQAQAAAdQAAAeAQAQQAQARAZAAQAZAAAQgRQAQgRAAgdQAAgdgQgQQgQgRgZAAQgZAAgQARgAgthmQgRgRAAgeIAAgEIAcAAQAAAdAiAAQAjAAAAgdIAcAAIAAAEQAAAegRARQgRASgdAAQgcAAgRgSg");
	this.shape_67.setTransform(248.6,39.5);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#EF7D00").s().p("AAmCHQgkAAgTgRQgTgSAAgpIAAhdIgeAAIAAgvIAeAAIAAg1IA2AAIAAA1IAxAAIAAAvIgxAAIAABeQAAAPAGAHQAHAGAPAAIAVAAIAAAvg");
	this.shape_68.setTransform(227.8,41.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#EF7D00").s().p("AhBBuIAAjYIA4AAIAAApQALgUATgMQAVgMAYAAIAAA7IgQAAQg7AAAAA3IAABpg");
	this.shape_69.setTransform(213.3,43.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#EF7D00").s().p("Ag2BiQgZgOgOgZQgNgZAAgiQAAghANgZQAOgaAZgNQAZgNAeAAQAgAAAYANQAYANANAYQANAXABAeQAAAHgCAKIieAAQABAdAOAOQAOAOAXAAQARAAANgJQANgKADgQIA7AAQgEAXgPASQgNASgXAKQgWAKgbAAQgeAAgZgNgAgjg1QgPAOgCAbIBoAAQABgbgPgOQgPgNgWAAQgVAAgPANg");
	this.shape_70.setTransform(192,43.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#EF7D00").s().p("AgjCXIAAipIgbAAIAAgvIAbAAIAAgFQABgmAVgVQAWgVArAAIALAAIAAAwQgYgCgKAJQgKAJAAAVIAAAAIAsAAIAAAvIgsAAIAACpg");
	this.shape_71.setTransform(172.8,39.5);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#EF7D00").s().p("Ag4BiQgZgOgPgZQgOgZAAgiQAAghAOgZQAPgaAZgNQAZgNAfAAQAgAAAZANQAZANAPAaQAOAZAAAhQAAAigOAZQgPAZgZAOQgZANggAAQgfAAgZgNgAgmguQgQAQAAAeQAAAfAQAQQAQAQAWAAQAXAAAQgQQAQgQAAgfQAAgegQgQQgQgRgXAAQgWAAgQARg");
	this.shape_72.setTransform(153.3,43.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#EF7D00").s().p("Ag4BiQgZgOgPgZQgOgZAAgiQAAghAOgZQAPgaAZgNQAZgNAfAAQAgAAAZANQAZANAPAaQAOAZAAAhQAAAigOAZQgPAZgZAOQgZANggAAQgfAAgZgNgAgmguQgQAQAAAeQAAAfAQAQQAQAQAWAAQAXAAAQgQQAQgQAAgfQAAgegQgQQgQgRgXAAQgWAAgQARg");
	this.shape_73.setTransform(119.7,43.8);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#EF7D00").s().p("AgbCbIAAjYIA3AAIAADYgAgYhmQgJgJAAgMQAAgOAJgIQAKgJAOAAQAPAAAKAJQAJAIAAAOQAAAMgJAJQgKAIgPAAQgOAAgKgIg");
	this.shape_74.setTransform(93.4,39.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#EF7D00").s().p("Ag2BiQgZgOgOgZQgOgZAAgiQAAghAOgZQAOgaAZgNQAYgNAfAAQAgAAAYANQAYANANAYQANAXABAeQAAAHgCAKIieAAQABAdAOAOQAOAOAXAAQARAAANgJQANgKADgQIA7AAQgEAXgOASQgOASgXAKQgWAKgbAAQgfAAgYgNgAgjg1QgPAOgCAbIBoAAQABgbgPgOQgPgNgWAAQgWAAgOANg");
	this.shape_75.setTransform(76,43.8);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#EF7D00").s().p("AhBBuIAAjYIA3AAIAAApQALgUAVgMQAUgMAYAAIAAA7IgQAAQg8AAAAA3IAABpg");
	this.shape_76.setTransform(56.7,43.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#EF7D00").s().p("AgjCKIhhkTIA7AAIBJDaIBKjaIA7AAIhiETg");
	this.shape_77.setTransform(33.8,40.8);

	this.instance_1 = new lib.formular1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(556,15,0.111,0.111);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.instance},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(484,124,972,252);
// library properties:
lib.properties = {
	width: 970,
	height: 250,
	fps: 24,
	color: "#EEEEEE",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/970X250_atlas_.png", id:"970X250_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;